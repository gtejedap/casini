module Casini
    ( parse
    ) where

import Data.Char
import Control.Applicative
import System.IO
import Control.Monad
import Data.List

type Section = (String, [([Char], [Char])])

newtype Parser a = Parser
  { runParser:: String -> Maybe (String, a)
  }

instance Functor Parser where
  fmap f (Parser p) =
    Parser $
    \input -> do
      (input', x) <- p input
      Just(input', f x)

instance Applicative Parser where
  pure x = Parser $ \input -> Just (input, x)
  (Parser f) <*> (Parser p) =
    Parser $
    \input -> do
      (input', f') <- f input
      (input'', x) <- p input'
      Just (input'', f' x)

instance Alternative Parser where
  empty = Parser $ const Nothing
  (Parser a) <|> (Parser b) =
    Parser $ \input -> a input <|> b input

parse :: String -> Maybe (String, [Section])
parse = runParser section

section :: Parser [Section]
section = e **>
          many ((,) <$> (header <** e) <*> many (property <** e))
  where
    e = many (spanP isIgnored **> charP '\n') <** spanP isIgnored

header :: Parser String
header = charP '[' *> spanP isAlphaNum <* charP ']'

property :: Parser ([Char], [Char])
property = (,) <$>
           (spanP isKey <* charP '=') <*>
           spanP isProp

sep :: Parser [Char]
sep = many $ spanP isIgnored *> charP '\n' <* spanP isIgnored

(<**) :: Parser a -> Parser b -> Parser a
(Parser a) <** (Parser b) = Parser f
  where
    f input = do
      case a input of
        Nothing -> Nothing
        m@(Just (rest, x)) -> case b rest of
                            Just (rest', _) -> Just (rest', x)
                            Nothing -> m

(**>) :: Parser a -> Parser b -> Parser b
(Parser a) **> (Parser b) = Parser f
  where
    f input = do
      case a input of
        Nothing -> b input
        Just (rest, _) -> b rest

isIgnored :: Char -> Bool
isIgnored c = c == ' ' ||
              c == '\t'

-- |Predicate to match any ASCII char less [, ], \n and =
isKey :: Char -> Bool
isKey c = c /= '=' &&
          c /= '\n' &&
          c /= '[' &&
          c /= ']' &&
          isAscii c

-- |Predicate to match any ASCII char less [, ], \n
isProp :: Char -> Bool
isProp c = c /= '\n' &&
           c /= '[' &&
           c /= ']' &&
           isAscii c

stringP :: String -> Parser String
stringP str = Parser f
  where
    f [] = Nothing
    f input = runParser (traverse charP str) input

spanP :: (Char -> Bool) -> Parser [Char]
spanP p = Parser f
  where f i = let (m, rest) = span p i
              in Just (rest, m)

charP :: Char -> Parser Char
charP c = Parser f
  where f [] = Nothing
        f (x:xs)
          | x == c = Just (xs, x)
          | otherwise = Nothing

main :: IO ()
main = do
  handle <- openFile "test.ini" ReadMode
  content <- hGetContents handle
  case runParser section content of
    Just ("", s) -> print s
    Just (xs, _) -> error $ "Incomplete -> " ++ xs
    Nothing -> error "An error has ocurred"

  hClose handle
